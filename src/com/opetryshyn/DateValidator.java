package com.opetryshyn;

import java.util.Date;

public interface DateValidator {
    boolean isValid(String dateStr);

    Date parse(String s);

    String getDateFormat();
}
