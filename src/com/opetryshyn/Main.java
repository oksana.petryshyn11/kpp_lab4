package com.opetryshyn;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class Main {
    public static final String FILENAME = "input.txt";
    private static final List<DateValidator> validators = new ArrayList<>();
    private static String text;

    static {
        validators.add(new DateValidatorUsingDateFormat("MM/dd/yyyy"));
        validators.add(new DateValidatorUsingDateFormat("dd/MM/yyyy"));
        validators.add(new DateValidatorUsingDateFormat("MM.dd.yyyy"));
        validators.add(new DateValidatorUsingDateFormat("dd.MM.yyyy"));
        validators.add(new DateValidatorUsingDateFormat("yyyy-mm-dd"));
        validators.add(new DateValidatorUsingDateFormat("yyyy-dd-mm"));
    }

    public static void main(String[] args) {
        try {
            List<String> datesString = lookForDataInFile(FILENAME);
            System.out.println("Found dates: ");
            System.out.println(datesString);

            List<LocalDate> datesSorted = datesString.stream().map(Main::convertDatesFromStringToLocalDate).sorted().toList();

            System.out.println();
            System.out.println("Range of dates:");
            System.out.println("From: " + datesSorted.get(0));
            System.out.println("To: " + datesSorted.get(datesSorted.size() - 1));

            System.out.println(text);
        } catch (FileNotFoundException | ParseException e) {
            System.out.println("An error occurred: " + e.getMessage());
        }
    }


    private static List<String> lookForDataInFile(String filename) throws FileNotFoundException, ParseException {
        List<String> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        Scanner scanner = new Scanner(new File(filename));
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            for (String token : line.split(" ")) {
                boolean isDate = false;
                for (DateValidator validator : validators) {
                    if (validator.isValid(token)) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat(validator.getDateFormat());
                        Date date = validator.parse(token);
                        Date tomorrow = new Date(date.getTime() + (1000 * 60 * 60 * 24));
                        String newDate = dateFormat.format(tomorrow);

                        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
                        String newToken = newDate + "(" + simpleDateformat.format(tomorrow) + ")";
                        sb.append(newToken).append(" ");
                        result.add(token);
                        isDate = true;
                        break;
                    }
                }
                if (!isDate) {
                    sb.append(token).append(" ");
                }
            }
        }
        text = sb.toString();
        return result;
    }

    private static LocalDate convertDatesFromStringToLocalDate(String datesString) {
        for (DateValidator validator : validators) {
            if (validator.isValid(datesString)) {
                return convertToLocalDate(validator.parse(datesString));
            }
        }
        return null;
    }

    public static LocalDate convertToLocalDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }
}